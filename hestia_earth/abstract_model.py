class Model():
    """Model class. This is an abstract class from which inherit all the models defined in Hestia. It contains a set
    of common methods and instance variables that are typically used by every model."""

    def __init__(self):
        """Constructor method.
            Arguments:
                None
            Returns instance."""
