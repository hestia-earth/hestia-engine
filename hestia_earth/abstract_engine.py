import copy
from collections import OrderedDict
from concurrent.futures import ThreadPoolExecutor


class Engine():
    """Engine class. This is the engine abstract class for the commn methods of the calculation and
    gap filling engine."""
    def __init__(self):
        """Constructor method.
            Arguments:
                None
            Returns instance."""

        self.name = None
        self.hierarchies = []
        self.cycle = None
        self.site = None
        self.representation = None
        self.output = None

    def clean_field(self, clean_fields, key):
        """Method that eliminates the non-used fields of the emissions output nodes dictionaries.
            Arguments:
                clean_fields: The dictionary where the field to be cleaned is in.
                key: the key of the field to be cleaned.
            Returns a dictionary without the unused fields."""

        if isinstance(clean_fields[key], list) and not clean_fields[key] == []:
            for object in clean_fields[key]:
                if not isinstance(object, str) and not isinstance(object, int) and not isinstance(object, float):
                    self.clean_crawler_fields(object)
        elif isinstance(clean_fields[key], dict) and not clean_fields[key] == {}:
            self.clean_crawler_fields(clean_fields[key])
        else:
            if clean_fields[key] == "" or clean_fields[key] is None or clean_fields[key] == [] \
                    or clean_fields[key] == {} or clean_fields[key] == "-":
                del (clean_fields[key])

    def clean_crawler_fields(self, mobject):
        """Method that eliminates the non-used fields of the emissions output nodes dictionaries.
            Arguments:
                mobject: The output emission node.
            Returns a dictionary without the unused fields."""

        clean_fields = mobject
        for key in list(clean_fields.keys()):
            self.clean_field(clean_fields, key)
        return clean_fields

    def converter_edict(self, object):
        """Method that converts a dictionary to an EngineDict if it is a dictionary, or a list of dictionaries to a list
        of EngineDicts. If it is not a dictionary or a collection of dictionaries returns the object itself.
            Arguments:
                object: The object to be converted to EngineDict.
            Returns the object converted to EngineDict or the object otherwise."""

        if isinstance(object, dict):
            EngineDict(object)
            return self.converter_crawler_edict(object)
        elif isinstance(object, list):
            return [self.converter_edict(item) for item in object]
        else:
            return object

    def converter_crawler_edict(self, object):
        """Method that converts all the dictionaries in a cycle to EngineDict.
            Arguments:
                object: A Hestia cycle.
            Returns a cycle where all the dictionaries are EngineDict."""

        mobject = EngineDict(object)
        for key in list(mobject.keys()):
            mobject[key] = self.converter_edict(mobject[key])

        return mobject

    def converter_dict(self, object):
        """Method that converts an EngineDict to a dictionary if it is an EngineDict, or a list (or elist) of
        EngingeDicts to a list of dictionaries. If it is not a dictionary or a collection of dictionaries returns the
        object itself.
            Arguments:
                object: The object to be converted to dictionary.
            Returns the object converted to dictionary or the object otherwise."""

        if isinstance(object, EngineDict):
            dict(object)
            return self.converter_crawler_dict(object)
        elif isinstance(object, list) or isinstance(object, EngineList):
            return [self.converter_dict(item) for item in object]
        else:
            return object

    def converter_crawler_dict(self, object):
        """Method that converts all the EngineDict in a cycle to dictionaries.
            Arguments:
                object: A Hestia cycle.
            Returns a cycle where all the EngineDict are dictionaries."""

        mobject = dict(object)
        for key in list(mobject.keys()):
            mobject[key] = self.converter_dict(mobject[key])

        return mobject

    def converter_dict_to_list(self, node, key):
        """Method that converts a key of a node (that is a dictionary) into a list defined in Hestia's Schema.
            Arguments:
                node: A Hestia node
                key: A key of the node
            Returns nothing."""

        if key in node:
            node[key] = node[key].evalues()

    def converter_list_to_dict(self, node, key):
        """Method that converts a subkey of a node (that is a dictionary) into a list defined in Hestia's Schema.
            Arguments:
                node: A Hestia node
                key: A subkey of the node
            Returns nothing."""

        if key in node:
            elist = EngineList(node[key])
            node[key] = elist.to_edict()

    def converter_subdict_to_list(self, node, key):
        """Method that converts a key of the subnodes of a node (that are lists) into a dictionary to easily access
        data.
            Arguments:
                node: A Hestia node
                key: A key of the subnodes
            Returns nothing."""

        subnodes = list(node.values())
        for subnode in subnodes:
            if key in subnode:
                self.converter_dict_to_list(subnode, key)

    def converter_subsubdict_to_list(self, node, subkey, subsubkey):
        """Method that converts a subsubkey of the subnodes of a node (that are lists) into a dictionary to easily
        access data.
            Arguments:
                node: A Hestia node
                subkey: A subkey of the subnodes
            Returns nothing."""

        subnodes = list(node.values())
        for subnode in subnodes:
            if subkey in subnode and subsubkey in subnode[subkey]:
                self.converter_dict_to_list(subnode[subkey], subsubkey)

    def converter_sublist_to_dict(self, node, key):
        """Method that converts a subkey of the subnodes of a node (that are lists) into a dictionary to easily access
        data.
            Arguments:
                node: A Hestia node
                key: A subkey of the subnodes
            Returns nothing."""

        subnodes = list(node.values())
        for subnode in subnodes:
            if key in subnode:
                self.converter_list_to_dict(subnode, key)

    def converter_subsublist_to_dict(self, node, subkey, subsubkey):
        """Method that converts a subsubkey of the subnodes of a node (that are lists) into a dictionary to easily
        access data.
            Arguments:
                node: A Hestia node
                subkey: A subkey of the subnodes
                subsubkey: A subsubkey of the subnodes
            Returns nothing."""

        subnodes = list(node.values())
        for subnode in subnodes:
            if subkey in subnode and subsubkey in subnode[subkey]:
                self.converter_list_to_dict(subnode[subkey], subsubkey)

    def import_data(self, cycle, site=None):
        """Method that imports a jsonld file into the representation required to perform the Hestia calculations.
            Arguments:
                file: A jsonld file
            Returns nothing."""

        self.cycle = copy.deepcopy(cycle)

        self.site = copy.deepcopy(site) if site is not None else \
            (copy.deepcopy(cycle['site']) if 'site' in cycle else None)
        self.cycle["site"] = self.site
        self.cycle = self.converter_crawler_edict(self.cycle)

        if 'site' in self.cycle and self.cycle['site'] is not None:
            self.converter_list_to_dict(self.cycle['site'], 'measurements')
            self.converter_subsublist_to_dict(self.cycle['site']['measurements'], 'term', 'defaultProperties')
        if 'inputs' in self.cycle and self.cycle['inputs'] is not None:
            self.converter_list_to_dict(self.cycle, 'inputs')
            self.converter_sublist_to_dict(self.cycle['inputs'], 'properties')
            self.converter_sublist_to_dict(self.cycle['inputs'], 'inventory')
            self.converter_subsublist_to_dict(self.cycle['inputs'], 'term', 'defaultProperties')
        if 'products' in self.cycle and self.cycle['products'] is not None:
            self.converter_list_to_dict(self.cycle, 'products')
            self.converter_sublist_to_dict(self.cycle['products'], 'properties')
            self.converter_sublist_to_dict(self.cycle['products'], 'inventory')
            self.converter_subsublist_to_dict(self.cycle['products'], 'term', 'defaultProperties')
        if 'emissions' in self.cycle and self.cycle['emissions'] is not None:
            self.converter_list_to_dict(self.cycle, 'emissions')
            self.converter_sublist_to_dict(self.cycle['emissions'], 'properties')
            self.converter_sublist_to_dict(self.cycle['emissions'], 'inventory')
            self.converter_subsublist_to_dict(self.cycle['emissions'], 'term', 'defaultProperties')
        if 'practices' in self.cycle and self.cycle['practices'] is not None:
            self.converter_list_to_dict(self.cycle, 'practices')

        self.representation = self.cycle
        self.safe_representation = self.trasnform_engine_dict(self.cycle)

        return self.representation

    def export_data(self):
        """Method that exports the Engine representation into the Hestia Schema.
            Arguments:
                None
            Returns a dict representing the recalculated cycle."""

        self.output = copy.deepcopy(self.representation)

        if 'site' in self.cycle and self.cycle['site'] is not None:
            self.converter_subsubdict_to_list(self.output['site']['measurements'], 'term', 'defaultProperties')
            self.converter_dict_to_list(self.output['site'], 'measurements')
        if 'inputs' in self.cycle and self.cycle['inputs'] is not None:
            self.converter_subsubdict_to_list(self.output['inputs'], 'term', 'defaultProperties')
            self.converter_subdict_to_list(self.output['inputs'], 'inventory')
            self.converter_subdict_to_list(self.output['inputs'], 'properties')
            self.converter_dict_to_list(self.output, 'inputs')
        if 'products' in self.cycle and self.cycle['products'] is not None:
            self.converter_subsubdict_to_list(self.output['products'], 'term', 'defaultProperties')
            self.converter_subdict_to_list(self.output['products'], 'inventory')
            self.converter_subdict_to_list(self.output['products'], 'properties')
            self.converter_dict_to_list(self.output, 'products')
        if 'emissions' in self.cycle and self.cycle['emissions'] is not None:
            self.converter_subsubdict_to_list(self.output['emissions'], 'term', 'defaultProperties')
            self.converter_subdict_to_list(self.output['emissions'], 'inventory')
            self.converter_subdict_to_list(self.output['emissions'], 'properties')
            self.converter_dict_to_list(self.output, 'emissions')
        if 'practices' in self.cycle and self.cycle['practices'] is not None:
            self.converter_dict_to_list(self.output, 'practices')

        return self.converter_crawler_dict(self.output)

    def run(self):
        """Method that orchestrates the execution of hierarchies of the engine.
            Arguments:
                None
            Returns nothing."""

        hierarchies = self.hierarchies if self.hierarchies else []
        with ThreadPoolExecutor() as executor:
            executor.map(lambda hierarchy: hierarchy.process_hierarchy(self.representation), hierarchies)

        for hierarchy in self.hierarchies:
            if hierarchy.chosen_model is not None:
                hierarchy.update_representation(self.representation)

    def trasnform_engine_dict(self, representation):
        """Method to convert all the nested dictionaries to EngineDict.
            Arguments:
                key: a cycle representation
            Returns a cycle representation with all the dictionaries converted to EngineDict."""

        endict = EngineDict(representation)

        for key, node in endict.items():

            if isinstance(node, dict) or isinstance(node, EngineDict):
                endict[key] = self.trasnform_engine_dict(node)

            if isinstance(node, list) or isinstance(node, EngineList):
                for i, v in enumerate(node):
                    if isinstance(v, dict):
                        endict[key][i] = self.trasnform_engine_dict(v)

        return endict


class EngineDict(OrderedDict):
    """EngineDict class. This class creates dictionaries that do not throw error exceptions when we try to access keys
    that are not in the dictionary. This class also extends dictionaries in order to covert to list EngineDict
    itself."""
    def __missing__(self, key):
        """Method that returns an empty dictionary when a key is not fund.
            Arguments:
                key: key of a dictionary
            Returns empty dictionary."""

        value = type(self)()
        return value

    def evalues(self):
        """Method to generate an EngineList from an EngineDict.
            Arguments:
                None
            Returns EngineList."""

        values = EngineList()
        for element in self.__iter__():
            if isinstance(self.__getitem__(element), list):
                values.extend(self.__getitem__(element))
            else:
                values.append(self.__getitem__(element))

        return values


class EngineList(list):
    """EngineList class. This class also extends lists in order to covert them easily to EngineDict, that is the data
    structure used for the Engine representation."""
    def to_edict(self):
        """Method to generate an EngineDict from an EngineList.
            Arguments:
                None
            Returns EngineDict."""

        items = EngineDict()

        for element in self.__iter__():
            if element['term']['@id'] not in items:
                items[element['term']['@id']] = element
            elif isinstance(items[element['term']['@id']], list):
                items[element['term']['@id']].append(element)
            else:
                items[element['term']['@id']] = [items[element['term']['@id']], element]

        return items
