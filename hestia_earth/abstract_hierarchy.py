import os
import json
from hestia_earth.abstract_engine import EngineList


class Hierarchy():
    """Model_Hierarchy class. It contains a set of common methods and instance variables that are typically used by
    every hierarchy. A hierarchy defines the order to run models in Hestia."""

    context = None

    def __init__(self):
        """Constructor method.
            Arguments:
                None
            Returns instance."""

        self.term = None
        self.chosen_model = None

    def process_hierarchy(self, cycle):
        """Method that procesess the hierarchy through all the models for a given cycle. It is overwritten in for
        different kind of engines
            Arguments:
                cycle: This is a cycle in the representation required by the engine.
                (Different from the Schema representation)
            Returns a file object."""

        pass

    def update_representation(self, cycle):
        """Method that checks if the model has enough data to do the calculation.
            Arguments:
                cycle: This is a cycle in the representation required by the calculation engine.
                (Different from the Schema representation)
            Returns boolean."""

        pass

    def import_term(self, directory, file):
        """Method that imports the Hestia glossary information for the emission calculated in the hierarchy.
            Arguments:
                directory: The folder where the file is stored.
                file: The name of the file.
            Returns a file object."""

        with open(os.path.join(directory, file)) as f:
            self.term = json.load(f)

        if 'defaultProperties' in self.term:
            self.term['defaultProperties'] = EngineList(self.term['defaultProperties']).to_edict()
