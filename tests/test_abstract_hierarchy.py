import unittest
import json
from .utils import fixtures_path

from hestia_earth.abstract_hierarchy import Hierarchy


class TestAbstractHierarchy(unittest.TestCase):
    def test_import_term(self):
        filename = 'ch4ToAirCropResidueBurning.jsonld'

        with open(f"{fixtures_path}/term_imported.jsonld") as jsonld_file:
            expected = json.load(jsonld_file)

        hierarchy = Hierarchy()
        hierarchy.import_term(fixtures_path, filename)

        self.assertEqual(hierarchy.term, expected)


if __name__ == '__main__':
    unittest.main()
