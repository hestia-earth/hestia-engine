import unittest
import json
import copy
from .utils import fixtures_path

from hestia_earth.abstract_engine import Engine


class TestAbstractEngine(unittest.TestCase):
    def setUp(self):
        self.engine = Engine()

        with open(f"{fixtures_path}/cycle.jsonld") as jsonld_file:
            self.cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/site.jsonld") as jsonld_file:
            self.site = json.load(jsonld_file)

    def test_engine_export_data(self):
        with open(f"{fixtures_path}/calculated.jsonld", 'r') as f:
            expected = json.load(f)

        self.engine.import_data(self.cycle, self.site)
        self.engine.run()
        data = self.engine.clean_crawler_fields(self.engine.export_data())

        self.assertEqual(expected, data)

    def test_side_effects_1(self):
        cycle_before = copy.deepcopy(self.cycle)
        site_before = copy.deepcopy(self.site)

        self.engine.import_data(self.cycle, self.site)
        self.engine.run()

        self.assertEqual(cycle_before, self.cycle)
        self.assertEqual(site_before, self.site)

    def test_side_effects_2(self):
        cycle_before = copy.deepcopy(self.cycle)
        site_before = copy.deepcopy(self.site)
        cycle_before['site'] = site_before

        cycle_test = copy.deepcopy(self.cycle)
        site_test = copy.deepcopy(self.site)
        cycle_test['site'] = site_test

        self.engine.import_data(cycle_test)
        self.engine.run()

        self.assertEqual(cycle_before, cycle_test)

    def test_non_mandatory_fields(self):
        del(self.cycle['site'])
        del(self.cycle['inputs'])
        del(self.cycle['products'])
        del(self.cycle['emissions'])
        del(self.cycle['practices'])

        self.engine.import_data(self.cycle)
        self.engine.run()
        data = self.engine.clean_crawler_fields(self.engine.export_data())

        self.assertEqual(self.cycle, data)

    def test_ghost_keys_1(self):
        with open(f"{fixtures_path}/calculated.jsonld", 'r') as f:
            expected = json.load(f)

        self.engine.import_data(self.cycle, self.site)
        self.engine.representation['ghost_key'] = ""
        self.engine.safe_representation['inputs']['ghost_key']
        self.engine.run()
        data = self.engine.clean_crawler_fields(self.engine.export_data())

        self.assertEqual(expected, data)

    def test_ghost_keys_2(self):
        with open(f"{fixtures_path}/calculated.jsonld", 'r') as f:
            expected = json.load(f)

        self.engine.import_data(self.cycle, self.site)
        self.engine.run()
        data = self.engine.clean_crawler_fields(self.engine.export_data())
        node = self.engine.trasnform_engine_dict(data)
        node['inputs'][0]['ghost_key']

        self.assertEqual(expected, node)

    def test_ghost_keys_3(self):
        with open(f"{fixtures_path}/calculated.jsonld", 'r') as f:
            expected = json.load(f)

        self.engine.import_data(self.cycle, self.site)
        self.engine.safe_representation['inputs']['ghost_key']
        self.engine.run()

        self.assertEqual(expected, self.engine.export_data())

    def test_nested_assignation(self):
        self.engine.import_data(self.cycle, self.site)

        self.engine.representation['products']['wheatGrain']['term']['name'] = "test"
        assigned = self.engine.representation['products']['wheatGrain']['term']['name']

        self.assertEqual(assigned, "test")


if __name__ == '__main__':
    unittest.main()
