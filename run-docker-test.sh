#!/bin/sh
docker build -t hestia-engine:test -f tests/Dockerfile .
docker run --rm -v ${PWD}/coverage:/app/coverage hestia-engine:test
