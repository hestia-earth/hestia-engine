# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.17](https://gitlab.com/hestia-earth/hestia-engine/compare/v0.0.16...v0.0.17) (2020-11-11)


### Features

* **abstract_engine:** add extended representation for site ([4d02aae](https://gitlab.com/hestia-earth/hestia-engine/commit/4d02aaed47bf5aec6d15cfc57846dfd32c6f15be))

### [0.0.16](https://gitlab.com/hestia-earth/hestia-engine/compare/v0.0.15...v0.0.16) (2020-11-05)

### [0.0.15](https://gitlab.com/hestia-earth/hestia-engine/compare/v0.0.14...v0.0.15) (2020-10-30)


### Bug Fixes

* allow parallel update ([6becfd0](https://gitlab.com/hestia-earth/hestia-engine/commit/6becfd09e3965d6eaec29f8b1aaf11baed65a63d))

### [0.0.14](https://gitlab.com/hestia-earth/hestia-engine/compare/v0.0.13...v0.0.14) (2020-10-30)


### Features

* **abstract_engine:** return standard output ([87a7ed9](https://gitlab.com/hestia-earth/hestia-engine/commit/87a7ed907a8928b3dfb92c0429d03c0a9df45040))
* **abstract_engine:** return standard output ([4ba876c](https://gitlab.com/hestia-earth/hestia-engine/commit/4ba876c0575055b7300eb3172d92ecc821184cbd))

### [0.0.13](https://gitlab.com/hestia-earth/hestia-engine/compare/v0.0.12...v0.0.13) (2020-10-28)


### Bug Fixes

* fix import term ([ed55b6f](https://gitlab.com/hestia-earth/hestia-engine/commit/ed55b6fd6d51eeda4e61048be9c90baa327c46f0))

### [0.0.12](https://gitlab.com/hestia-earth/hestia-engine/compare/v0.0.11...v0.0.12) (2020-10-26)


### Features

* **abstract_engine:** add extended representation ([e734460](https://gitlab.com/hestia-earth/hestia-engine/commit/e734460b41f17adbfb95bdfc083482b851566c5f))
* **engine:** run hierarchies in parallel ([0177a79](https://gitlab.com/hestia-earth/hestia-engine/commit/0177a793fab34d2ecb098a81df1e831dbc63104b))

### [0.0.11](https://gitlab.com/hestia-earth/hestia-engine/compare/v0.0.10...v0.0.11) (2020-10-24)


### Bug Fixes

* **abstract_engine:** convert all dictionaries in representation to EngDict ([7ad69a7](https://gitlab.com/hestia-earth/hestia-engine/commit/7ad69a75c9b1caa7c5d93d458541b1f115624f9d)), closes [#5](https://gitlab.com/hestia-earth/hestia-engine/issues/5)

### [0.0.10](https://gitlab.com/hestia-earth/hestia-engine/compare/v0.0.9...v0.0.10) (2020-09-07)


### Bug Fixes

* **abstract_engine:** fix bug in EngineDict ([b85a06d](https://gitlab.com/hestia-earth/hestia-engine/commit/b85a06ddc394034d7b8ead39624d05651be2f43b))

### [0.0.9](https://gitlab.com/hestia-earth/hestia-engine/compare/v0.0.8...v0.0.9) (2020-09-06)


### Features

* **abstract_engine:** add EngineList class ([ec6fe58](https://gitlab.com/hestia-earth/hestia-engine/commit/ec6fe5816a29672cd8aff45bdc47299f5d3104a7))
* **abstract_engine:** add evalues() method to SafeDict ([33c68b1](https://gitlab.com/hestia-earth/hestia-engine/commit/33c68b179c4acee29138db8ccaf28dd630a45dd7))

### [0.0.8](https://gitlab.com/hestia-earth/hestia-engine/compare/v0.0.7...v0.0.8) (2020-09-05)


### Bug Fixes

* **abstract_engine:** allow representation with several nodes in the same key ([3713652](https://gitlab.com/hestia-earth/hestia-engine/commit/3713652f001917f3ab8fbd4385290c2981f55611))
* **abstract_engine:** do not add to the safe dictionary the ghost key ([1775306](https://gitlab.com/hestia-earth/hestia-engine/commit/177530600f7596616a8c4d9e7279cdb7dcbd4249))
* **abstract_engine:** fix safe dictionary with nested lists ([e9abb6f](https://gitlab.com/hestia-earth/hestia-engine/commit/e9abb6f7a9c4943f05a5d1b2a01fea97e75bd403))
* **abstract_engine:** remove empty nested dictionaries ([ea50d5e](https://gitlab.com/hestia-earth/hestia-engine/commit/ea50d5e85fed0a99b9ca8b880a28f7354f1d2eb1))

### [0.0.7](https://gitlab.com/hestia-earth/hestia-engine/compare/v0.0.6...v0.0.7) (2020-09-03)


### Features

* **SafeDict:** add safe dictionary ([dc1e6a5](https://gitlab.com/hestia-earth/hestia-engine/commit/dc1e6a5308b0968de9d49c66307bd22aba3b35f3))


### Bug Fixes

* **abstract_engine:** do not add to the safe dictionary the ghost key ([84ca747](https://gitlab.com/hestia-earth/hestia-engine/commit/84ca747901b23b2fd7a3b1df30051b661ac9a25b))
* **abstract_engine:** fix safe dictionary with nested lists ([3406372](https://gitlab.com/hestia-earth/hestia-engine/commit/3406372efc2f29e6f18d57ea9cc7fda933bbfc17))
* **abstract_engine:** remove empty nested dictionaries ([620e269](https://gitlab.com/hestia-earth/hestia-engine/commit/620e269528bc506233eff26cb7857cfa65dbb446))

### [0.0.6](https://gitlab.com/hestia-earth/hestia-engine/compare/v0.0.5...v0.0.6) (2020-08-27)


### Bug Fixes

* **abstract_engine:** fix internal side effect in internal site ([6f4a325](https://gitlab.com/hestia-earth/hestia-engine/commit/6f4a3254eb596fc65d0f364c39eb88c3d82784c7))

### [0.0.5](https://gitlab.com/hestia-earth/hestia-engine/compare/v0.0.4...v0.0.5) (2020-08-27)

### [0.0.4](https://gitlab.com/hestia-earth/hestia-engine/compare/v0.0.3...v0.0.4) (2020-08-26)


### Bug Fixes

* **abstract_engine:** fix params side effects ([079b2a2](https://gitlab.com/hestia-earth/hestia-engine/commit/079b2a218f10fc45ee636d9cd1f3fe6ba5ee1856)), closes [#2](https://gitlab.com/hestia-earth/hestia-engine/issues/2)

### [0.0.3](https://gitlab.com/hestia-earth/hestia-engine/compare/v0.0.2...v0.0.3) (2020-08-25)


### Features

* **abstract_engine:** add practices to representation ([7ed9018](https://gitlab.com/hestia-earth/hestia-engine/commit/7ed9018249d671ecc520d44d75ec3e27d443844f))

### [0.0.2](https://gitlab.com/hestia-earth/hestia-engine/compare/v0.0.1...v0.0.2) (2020-08-10)


### Bug Fixes

* **abstract_hierarchy:** change from calculate to process naming ([c08f1f5](https://gitlab.com/hestia-earth/hestia-engine/commit/c08f1f5a55959304bcca7a23b5e042980c80ba9b))


## 0.0.1 (2020-08-05)


### Features

* **abstract_engine:** add initial abstract engine ([6891336](https://gitlab.com/hestia-earth/hestia-engine/commit/689133639a7553dc9c08df81b265bb908cb5cd65))
* **abstract_engine:** add orchestration method ([42da56c](https://gitlab.com/hestia-earth/hestia-engine/commit/42da56cfcc3e6a3a892ee83f869bb2775787a452))
* **abstract_engine:** implement symmetric import/export representation ([a57d328](https://gitlab.com/hestia-earth/hestia-engine/commit/a57d328daddf8563181cf12dd16c7621d558a3e8))
* **abstract_engine:** remove empty fields from nodes ([a37047a](https://gitlab.com/hestia-earth/hestia-engine/commit/a37047ac734e2a5d59e1fbf4de5c2905b45fd959))
* **abstract_hierarchy:** add abstract_hierarchy ([f16ecb7](https://gitlab.com/hestia-earth/hestia-engine/commit/f16ecb7aab6267f5295a18c00bccfdbd143d5f4e))
* **abstract_model:** add abstract_model ([117230e](https://gitlab.com/hestia-earth/hestia-engine/commit/117230ec3821d469be1a9866a10f30b9a33e37fb))


### Bug Fixes

* **abstract_engine:** allow no new nodes (emissions) ([bfd892c](https://gitlab.com/hestia-earth/hestia-engine/commit/bfd892c7d5e04eec03f08824c75a6883f7475873))
* **abstract_engine:** initialize hierarchies as lists ([eb949ec](https://gitlab.com/hestia-earth/hestia-engine/commit/eb949ec2bd66174a706a02c8da7a8e4b71893b6a))
* **abstract_model:** define import encoding ([12d8232](https://gitlab.com/hestia-earth/hestia-engine/commit/12d823227608aabae6795d75513c5f0deaae47f5))
